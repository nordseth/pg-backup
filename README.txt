# pg-backup

based on https://github.com/appuio/postgresql-simple-backup-container/blob/master/Dockerfile_9.5

Envs

POSTGRESQL_USER=user
POSTGRESQL_PASSWORD=xxx
POSTGRESQL_SERVICE_HOST=localhost
POSTGRESQL_SERVICE_PORT=5432
POSTGRESQL_DATABASE=sampledb
BACKUP_DATA_DIR=/var/lib/pgsql/data
BACKUP_KEEP=5
BACKUP_MINUTE=0
BACKUP_HOUR=10,22

--

does pg_dump with gzip into backup data dir.

to restore use gzip and psql tool

DATE=2018-06-17-22-00
gzip -d $BACKUP_DATA_DIR/dump-${DATE}.sql.gz
export PGPASSWORD=$POSTGRESQL_PASSWORD
psql -U $POSTGRESQL_USER -h $POSTGRESQL_SERVICE_HOST -p $POSTGRESQL_SERVICE_PORT -d $POSTGRESQL_DATABASE -f $BACKUP_DATA_DIR/dump-${DATE}.sql
