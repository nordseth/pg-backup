FROM centos/postgresql-96-centos7
WORKDIR /app
COPY backup.sh run.sh ./
ENV POSTGRESQL_USER= \
	POSTGRESQL_PASSWORD= \
	POSTGRESQL_SERVICE_HOST=postgresql \
	POSTGRESQL_SERVICE_PORT=5432 \
	POSTGRESQL_DATABASE=sampledb \
	BACKUP_DATA_DIR=/var/lib/pgsql/data \
	BACKUP_KEEP=5 \
	BACKUP_MINUTE=* \
	BACKUP_HOUR=*

USER root

RUN yum -y update && yum clean all
RUN	yum -y install epel-release && \
	yum -y install install python python-devel python-pip mercurial && \
    yum clean all

# Install dev cron
RUN pip install -e hg+https://bitbucket.org/dbenamy/devcron#egg=devcron
RUN chmod -R 777 .

USER 1001

CMD ./run.sh